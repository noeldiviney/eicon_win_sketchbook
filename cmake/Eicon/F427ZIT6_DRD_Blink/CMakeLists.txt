SET(CCLL CMAKE_CURRENT_LIST_LINE)
if(${WIN32})
	set(DCMAKE_SH="CMAKE_SH-NOTFOUND")
	string(SUBSTRING ${CMAKE_CURRENT_SOURCE_DIR} 5 1 DRV)
	string(TOLOWER "${DRV}" DRV)
endif()
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}    CWD                          = ${CMAKE_CURRENT_SOURCE_DIR}") 
MESSAGE(STATUS "CMakeLists.txt line ${${CCLL}}    DRV                          = ${DRV}") 
cmake_minimum_required(VERSION 3.15.2)
set(CMAKE_VERSION                   3.15.3)
set(ARDUINO_VERSION                 1.8.13)
set(BOARD_MANUFACTURER              Eicon)
set(BOARD_MANUFACTURER_VERSION      1.0.1)
set(STM32_PLATFORM_VERSION          1.9.0)
set(BOARD_VARIANT                   F427ZIT6_DRD_Blink)
set(STM32CUBE_PATH                  /opt/tools/STM32Cube/STM32Cube_FW_F4_V1.24.1)
set(STM32_CPU                       F427ZIT6
set(DEVICE_SUFFIX                   xx)
set(GCC_SUFFIX                      )
if(${WIN32})
	set(GCC_SUFFIX                      .exe)
endif()
set(PROJECT_PATH                    ${CMAKE_SOURCE_DIR})
set(STM32_FIRMWARE_PATH             ${STM32CUBE_PATH})
set(PROJECT_NAME                    ${PROJECT_NAME})
set(SKETCH_NAME                     ${BOARD_VARIANT})
set(SKETCH_PATH                     ${DRV}:/msys64/opt/arduino-${ARDUINO_VERSION}/portable/sketchbook/CMake/${SKETCH_NAME})
set(PRJ_NAME                        ${BOARD_VARIANT})
string(SUBSTRING ${STM32_CPU} 0 4 MCU)
string(SUBSTRING ${STM32_CPU} 0 2 F_SERIES)
set(BOARD_TYPE                      STM32${MCU}${DEVICE_SUFFIX})               # MCU_FAMILY STM32F427xx           
set(MCU_LINE                        STM32${MCU}${DEVICE_SUFFIX})               # MCU_FAMILY STM32F4xx
set(MCU_FAMILY                      STM32${F_SERIES}${DEVICE_SUFFIX})          # MCU_FAMILY STM32F4xx
set(MCU_FAMILY_LC                   stm32${F_SERIES}${DEVICE_SUFFIX})
set(EICON_BOARD                     ${BOARD_VARIANT})
set(${BOARD_MANUFACTURER}_BOARD     ${BOARD_VARIANT})
set(ARDUINO_INSTALL_DIRECTORY       /opt/arduino-${ARDUINO_VERSION})
set(HARDWARE_PATH                   ${ARDUINO_INSTALL_DIRECTORY}/portable/packages)                                                      # $(ARDUINO_INSTALL_DIRECTORY}/${BOARD_MANUFACTURER}
set(BOARD_MANUFACTURER_PATH         ${HARDWARE_PATH}/${BOARD_MANUFACTURER}/hardware/stm32/${BOARD_MANUFACTURER_VERSION})
set(STM32_PLATFORM_PATH             ${HARDWARE_PATH}/STM32/hardware/stm32/${STM32_PLATFORM_VERSION})
set(STM32_TOOLS_PATH                ${HARDWARE_PATH}/STM32/tools)
#set(STM32_TOOLCHAIN_PATH            /opt/tools/arm-st/bin)
set(STM32_TOOLCHAIN_PATH            ${STM32_TOOLS_PATH}/xpack-arm-none-eabi-gcc/9.2.1-1.1/bin)

set(VARIANTS_PATH                   ${HARDWARE_PATH}/${BOARD_MANUFACTURER}/hardware/stm32/${BOARD_MANUFACTURER_VERSION}/variants)
set(MCU_LINKER_SCRIPT               ${VARIANTS_PATH}/${BOARD_VARIANT}/ldscript.ld)
set(ARDUINO_VERSION 10813)
#MESSAGE(STATUS "CMakeLists.txt line 8}     F_SERIES                  = ${F_SERIES}") 

include(CMake/Message-1.cmake)

#return()

include(CMake/STM32Toolchain.cmake)
#return()
include(CMake/ArduinoStm32Lib.cmake)
#return()
include(CMake/Message-2.cmake)
project(${PRJ_NAME} C CXX ASM)
add_library(arduinolib ${ARDUINOLIB_SOURCES})
add_executable(${PRJ_NAME}.elf ${SKETCH_CPP_SOURCES})
target_link_libraries(${PRJ_NAME}.elf arduinolib)
#set(CMAKE_EXE_LINKER_FLAGS -Wl,-Map=${PROJECT_SOURCE_DIR}/build/${PRJ_NAME}.map)
set(CMAKE_LINKER_FLAGS    -Wl,-Map=${PROJECT_SOURCE_DIR}/build/${PRJ_NAME}.map)
set(HEX_FILE ${PROJECT_SOURCE_DIR}/build/${PRJ_NAME}.hex)
set(BIN_FILE ${PROJECT_SOURCE_DIR}/build/${PRJ_NAME}.bin)
include(CMake/Message-3.cmake)
add_custom_command(TARGET F427ZIT6_DRD_Blink.elf POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} ARGS -Oihex $<TARGET_FILE:${PRJ_NAME}.elf> ${HEX_FILE}
        COMMAND ${CMAKE_OBJCOPY} ARGS -Obinary $<TARGET_FILE:${PRJ_NAME}.elf> ${BIN_FILE}
COMMENT "Building ${HEX_FILE} \nBuilding ${BIN_FILE}")
include(CMake/Message-4.cmake)
MESSAGE(STATUS End of CMakeLists.txt)
